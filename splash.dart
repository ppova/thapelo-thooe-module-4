import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: MyWidget(),
        ),
      ),
    );
  }
}

class MyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(
                  "https://media.istockphoto.com/id/1212537872/vector/full-length-of-cartoon-sick-people-in-medical-masks-standing-in-line-against-white-background.jpg?s=612x612&w=0&k=20&c=Aou52kwqNdBq4HsEyKn3E_n265fxVeNuNaom6V1glwc="),
              fit: BoxFit.cover)),
      padding: EdgeInsets.only(top: 50.0),
      child: Text(
        "Don't stand in line.",
        style: TextStyle(
            color: Colors.black.withOpacity(0.4),
            fontSize: 38.0,
            height: 1.4,
            fontWeight: FontWeight.w600),
        textAlign: TextAlign.center,
      ),
    );
  }
}
